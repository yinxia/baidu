FROM  node:latest

ADD . baidu-vue/

WORKDIR /baidu-vue

RUN yarn install

ENTRYPOINT ["yarn","run","serve"]