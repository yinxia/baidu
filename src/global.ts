export default {
  IMGURL: '/api/attach/pub/img/',
  _companyMessage: {
    // map: '',
    addressImg: require('@/assets/map.jpg'),
    // code: '',
    code: require('@/assets/aijiashuo.jpg'),
    address: '广州市天河区毓南路13号冠达商务中心E座111-112',
    phone: '020-23337353',
    email: 'aijia@bjike.com',
    website: 'https://www.bjike.com'
  },
  _otherMessage: {},

  getNavName() {
    return this._navName;
  },

  setNavName(value: any) {
    this._navName = value;
  },

  getCompanyMessage() {
    return this._companyMessage;
  },

  setCompanyMessage(value: any) {
    this._companyMessage = value;
  },

  getOtherMessage() {
    return this._otherMessage;
  },

  setOtherMessage(value: any) {
    this._otherMessage = value;
  },



  // 设置cookies
  setCookie(c_name: any, value: any, expire: any) {
    var date = new Date()
    date.setSeconds(date.getSeconds() + expire)
    // document.cookie = c_name + "=" + encodeURI(value) + "; expires=" + date.toGMTString()
    document.cookie = c_name + "=" + encodeURI(value) + "; expires=" + date.toUTCString()
  },
  // 获取cookies
  getCookie(c_name: any) {
    if (document.cookie.length > 0) {
      let c_start = document.cookie.indexOf(c_name + "=")
      if (c_start != -1) {
        c_start = c_start + c_name.length + 1
        let c_end = document.cookie.indexOf(";", c_start)
        if (c_end == -1) c_end = document.cookie.length
        return decodeURI(document.cookie.substring(c_start, c_end))
      }
    }
    return ""
  },
  // 删
  delCookie(c_name: any) {
    this.setCookie(c_name, "", -1)
  },
  // 原系统获取cookie
  getCooki(cname: any) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
      var c = ca[i].trim();
      if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
  }





}
