import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const mian = () => import('./views/mian.vue')
const indexsixdu = () => import('./views/indexSixdu.vue')
// const Recruits = () => import('./views/Recruits.vue')
const newsdetailed  = () => import('./components/newsdetailed.vue')
const casedetailed  = () => import('./components/casedetailed.vue')
const knowledgedetailed  = () => import('./components/knowledgedetailed.vue')
const servicedetails  = () => import('./components/servicedetails.vue')

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: mian,
      redirect:{name:'indexsixdu'},
      children:[
        {          
            path: 'indexsixdu',
            name: 'indexsixdu',
            component: indexsixdu
         
        },
        {
          path: 'newsdetailed',
          name: 'newsdetailed',
          component: newsdetailed
        },
        {
          path: 'casedetailed',
          name: 'casedetailed',
          component: casedetailed
        },
        {
          path: 'knowledgedetailed',
          name: 'knowledgedetailed',
          component: knowledgedetailed
        },
        {
          path: 'servicedetails',
          name: 'servicedetails',
          component: servicedetails
        }
      ]
    },
    
  ]
})
