import axios from './sethttp'  // 普通请求

/**
 *  当前机构信息
 * @export
 * @param {*} 
 */
export const currentInformation = () => {
    return axios.get('api/organization/pub/findOrganization')
};

/**
 *   轮播图列表
 * @export
 * @param {*} data ——类型
 */
export const slidesbannerList= (data) => {
    return axios.get('api/roundImg/pub/findListImg',{params:data})
};

/**
 *   新闻列表  首页精选
 * @export
 * @param {*} data 无
 */
export const newsListindex= () => {
    return axios.get('api/journalism/pub/selectedList')
};

/**
 *   新闻头条
 * @export
 * @param {*} data 无
 */
export const newsListindextop= () => {
    return axios.get('api/journalism/pub/headlinesList')
};





/**
 *   新闻列表  新闻中心
 * @export
 * @param {*} data 无
 */
export const newsListcenter= () => {
    return axios.get('api/journalism/pub/findPager')
};
/**
 *   案例  首页精选
 * @export
 * @param {*} data 无
 */
export const caseListindex= () => {
    return axios.get('api/case/pub/selectedList')
}


/**
 *   案例中心————列表
 * @export
 * @param {*} data ——无
 */
export const caseCanterList= () => {
    return axios.get('api/case/pub/findPager')
};


/**
 *   学习列表
 * @export
 * @param {*} data ——无
 */
export const learnListindex= () => {
    return axios.get('api/literature/pub/findPager')
};


/**
 *   服务列表
 * @export
 * @param {*} data ——none
 */
export const serviceListindex= () => {
    return axios.get('api/service/pub/findPager')
};



//   《=================================鹏诚结束=================================》


/**
 *  联系页保存
 * @export
 * @param {*} data ——保存数据
 */
export const addresssave = (data) => {
    return axios.post('api/organization/saveAddress', data)
};

/**
 *  保存轮播图
 * @export
 * @param {*} data ——保存类型
 */
export const saveBanners = (data) => {
    return axios.post('api/roundImg/save', data)
};

/**
 *  获取轮播图
 * @export
 * @param {*} data ——类型
 */
export const getbanners = (data) => {
    return axios.get('api/roundImg/findListImg',{params:data})
};

/**
 *  保存优秀员工
 * @export
 * @param {*} data ——保存信息
 */
export const excellentemployeeSave = (data) => {
    return axios.post('api/staff/save', data)
};

/**
 *  获取优秀员工列表
 * @export
 * @param {*} data -- none
 */
export const excellentemployeeList = () => {
    return axios.get('api/staff/findPager')
};

/**
 *  获取优秀员工--删除
 * @export
 * @param {*} data -- 员工id
 */
export const excellentemployeedelete = (data) => {
    return axios.post('api/staff/del',data)
};

/**
 *  获取优秀员表头
 * @export
 * @param {*} data -- none
 */
export const excellentemployeetitle = () => {
    return axios.get('api/staff/findPager')
};
/**
 *  保存公司优势
 * @export
 * @param {*} data -- 数据
 */
export const saveAdvantage = (data) => { 
    return axios.post('api/organization/saveAdvantage',data)
};

/**
 *  添加新闻
 * @export
 * @param {*} data -- 新闻数据
 */
export const journalismsave = (data) => {
    
    return axios.post('api/journalism/save',data)
};

/**
 *   新闻列表
 * @export
 * @param {*} data -- 分页
 */
export const newsinformationtype1 = (data) => {
    return axios.get('api/journalism/findPager1',{params:data});
};

/**
 *   资讯列表
 * @export
 * @param {*} data -- 分页
 */
export const newsinformationtype2 =(data) => {
    return axios.get('api/journalism/findPager2', { params: data });
};

/**
 *   新闻/资讯列表删除
 * @export
 * @param {*} data -- id
 */
export const newsinformationtypedelete =(data) => {
    return axios.post('api/journalism/del',data);
};



/**
 *   轮播图删除图片 
 * @export
 * @param {*} data -- 图片id
 */
export const deleteBannerimg =(data) => {
    return axios.post('api/roundImg/delImg',data);
};

/**
 *   公司简介保存 
 * @export
 * @param {*} data -- 保存简介
 */
export const companyprofileSave =(data) => {
    return axios.post('api/organization/saveSimple',data);
};

/**
 *   时间线保存
 * @export
 * @param {*} data -- 时间线json
 */
export const dataLinesave =(data) => {
    return axios.post('api/slot/saves',data);
};

/**
 *   时间线获取
 * @export
 * @param {*} data -- 时间线json
 */
export const dataLinelist =() => {
    return axios.get('api/slot/findList');
};

/**
 *   公司服务类型保存/编辑
 * @export
 * @param {*} data -- 服务名称
 */
export const typeOfservice =(data) => {
    return axios.post('api/serviceType/save',data);
};

/**
 *   公司服务类型--列表
 * @export
 * @param {*} data -- 无
 */
export const typeOfserviceList =() => {
    return axios.post('api/serviceType/findList');
};

/**
 *   公司服务内容保存/编辑
 * @export
 * @param {*} data -- 服务名称
 */
export const serviceContentsave =(data) => {
    return axios.post('api/serviceContent/save',data);
};

/**
 *   公司服务内容列表
 * @export
 * @param {*} data -- 无
 */
export const serviceContentList =() => {
    return axios.post('api/serviceType/findContent');
};

/**
 *   公司服务内容列表/删除
 * @export
 * @param {*} data -- 内容id
 */
export const serviceContentdelete =(data) => {
    return axios.post('api/serviceContent/del',data);
};

/**
 *   公司服务内容列表__通过类型id筛选
 * @export
 * @param {*} data -- 类型id
 */
export const serviceContentListselect =(data) => {
    return axios.get('api/serviceContent/findByTypeId',{params:data});
};


/**
 *   公司活动保存/编辑
 * @export
 * @param {*} data -- 活动内容
 */
export const activityManagementsave =(data) => {
    return axios.post('api/activity/save',data);
};

/**
 *   公司活动列表
 * @export
 * @param {*} data -- 搜索内容
 */
export const activityManagementList =() => {
    return axios.get('api/activity/findPager');
};

/**
 *   公司活动列表删除
 * @export
 * @param {*} data -- id
 */
export const activityManagementdelete =(data) => {
    return axios.post('api/activity/del',data);
};


/**
 *   合作机构添加
 * @export
 * @param {*} data -- 提交数据
 */
export const cooperativeInstitutionSave =(data) => {
    return axios.post('api/cooperation/save ',data);
};

/**
 *   合作机构添加列表删除
 * @export
 * @param {*} data -- 列表id
 */
export const cooperativeInstitutionDelete =(data) => {
    return axios.post('api/cooperation/del ',data);
};

/**
 *   合作机构添加列表
 * @export
 * @param {*} data -- 无
 */
export const cooperativeInstitutionList =() => {
    return axios.get('api/cooperation/findList');
};


/**
 *   获取主办方下拉
 * @export
 * @param {*} data -- 无
 */
export const sponsorList =() => {
    return axios.get('api/cooperation/findSponsor');
};
/**
 *   根据主办方id获取活动
 * @export
 * @param {*} data -- 主办方id
 */
export const getActivebysponsor =(data) => {
    return axios.get('api/activity/findList',{params:data});
};

/**
 *   订单保存
 * @export
 * @param {*} data -- 数据
 */
export const orderSave =(data) => {
    return axios.post('api/order/save',data);
};

/**
 *   订单列表
 * @export
 * @param {*} data -- 条件/分页
 */
export const orderList =(data) => {
    return axios.get('api/order/findList',{params:data});
};

/**
 *   订单详情
 * @export
 * @param {*} data -- id
 */
export const orderInformation =(data) => {
    return axios.get('api/order/findOne',{params:data});
};


/**
 *   订单删除
 * @export
 * @param {*} data -- 订单id
 */
export const orderDelete =(data) => {
    return axios.post('api/order/del',data);
};








