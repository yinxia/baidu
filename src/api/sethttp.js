import axios from 'axios';
const $http = axios.create()
$http.defaults.timeout = 5000;
$http.defaults.withCredentials = true;
$http.defaults.baseURL = process.env.VUE_APP_URL;
// console.log(process.env.VUE_APP_URL)
$http.interceptors.request.use((config) => { 
    config.headers = {
        
        'Content-Type': 'application/x-www-form-urlencoded'
    }
    // config.data = qs.stringify(config.data, {arrayFormat: 'repeat'});
    return config;
},  
    (error) => {
    return Promise.reject(error);   
});
$http.interceptors.response.use(data => {
       if(data.status==200){ 
            // this.$router.push({name:'login'})
           return data
       }
       return data
}, error => {
console.log(error)
});
export default $http
