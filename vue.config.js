module.exports={
  
  chainWebpack:config => {
    config.externals={
        'AMap': 'AMap'
       }
  },

    baseUrl:'./',
    outputDir:'dist',
    // 运行端口
    devServer: {
      port: 8092,
      proxy: {
          '/api': {
              target: 'http://yinxia138.com/',
              secure: false,  // 如果是https接口，需要配置这个参数
              changeOrigin: true,  //是否跨域
              pathRewrite: {
                '^/api': ''
              },  //需要rewrite的,
              ws: true,
          }
      },
      disableHostCheck: true
  },
    // 取消eslint检测
  lintOnSave: false, 
  productionSourceMap:false,
 
}